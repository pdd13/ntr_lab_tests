﻿using System;

namespace ntr_lab_tests
{
    public class Test1
    {
        private int _searchedValue;
        private int[] _inputArray;

        public int FindIndexOfFirstGreaterItem(int[] inputArray, int searchedValue)
        {
            GetSearchedValue(searchedValue);
            GetInputArray(inputArray);

            var index = GetSearchedIndex(); return index;
        }

        private void GetSearchedValue(int searchedValue)
        {
            _searchedValue = searchedValue;
            Console.Write($"\nSearched value: {_searchedValue}\n");
        }

        private void GetInputArray(int[] inputArray)
        {
            _inputArray = inputArray;

            Console.Write("Input array (value and index): \n\n");
            foreach (var x in _inputArray)
            {
                var valueOfItem = x;
                Console.Write($"{valueOfItem} ");
            }

            Console.WriteLine();
            var indexOfItem = 0;
            foreach (var x in _inputArray)
            {
                Console.Write($"{indexOfItem} ");
                indexOfItem += 1;
            }
        }

        public int GetSearchedIndex()
        {
            var indexOfFirstItem = 0;
            var valueOfFirstItem = _inputArray[indexOfFirstItem];
            var indexOfLastItem = _inputArray.Length - 1;
            var valueOfLastItem = _inputArray[indexOfLastItem];

            if (_searchedValue < valueOfFirstItem)
            {
                Console.WriteLine($"\n\nIndex of the first item: not found. Return {-1}");
                return -1;
            }

            if (_searchedValue > valueOfLastItem)
            {
                Console.WriteLine($"\n\nIndex of the first item: not found. Return {indexOfLastItem}");
                return indexOfLastItem;
            }

            Console.WriteLine("\n\nSearch process (index):\n");

            int indexLow = indexOfFirstItem;
            int indexHigh = indexOfLastItem + 1;
            int indexMiddle = 0;

            while (indexLow < indexHigh)
            {
                indexMiddle = indexLow + (indexHigh - indexLow) / 2;
                Console.WriteLine($"low = {indexLow} high = {indexHigh} middle = {indexMiddle}");

                if (_searchedValue <= _inputArray[indexMiddle]) indexHigh = indexMiddle;
                else indexLow = indexMiddle + 1;                
            }

            Console.WriteLine($"low = {indexLow} high = {indexHigh} middle = {indexMiddle}");
            Console.WriteLine($"\nIndex of the first item: {indexLow} ({_inputArray[indexLow]})");
            return indexLow;
        }
    }
}