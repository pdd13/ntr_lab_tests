﻿using System;

namespace ntr_lab_tests
{
    class Program
    {
        static void Main(string[] args)
        {
            // test 1

            Console.Write("\n> Test 1\n");

            var test1 = new Test1();

            int[] test1_array1 = { 1, 2, 3, 4, 5 };
            int[] test1_array2 = { 1, 2, 3, 7, 9 };
            int[] test1_array3 = { 1, 3, 7, 9 };

            test1.FindIndexOfFirstGreaterItem(test1_array1, 3);
            test1.FindIndexOfFirstGreaterItem(test1_array2, 5);
            test1.FindIndexOfFirstGreaterItem(test1_array3, 5);
            test1.FindIndexOfFirstGreaterItem(test1_array1, 10);
            test1.FindIndexOfFirstGreaterItem(test1_array1, 0);

            // test 2

            Console.Write("\n> Test 2\n");

            var test2 = new Test2();

            int[] test2_array1 = { -7, 100, 120, -7, -1, 150 };
            int[] test2_array2 = { -7, -7, -3 };
            int[] test2_array3 = { 7, 7, 3 };

            test2.FindLargestSum(test2_array1);
            test2.FindLargestSum(test2_array2);
            test2.FindLargestSum(test2_array3);

            // exit

            Console.Write("\n> Press Enter to exit ..."); Console.ReadKey();
        }
    }
}