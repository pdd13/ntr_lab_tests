﻿using System;

namespace ntr_lab_tests
{
    public class Test2
    {
        private int[] _inputArray;

        public int FindLargestSum(int[] inputArray)
        {
            GetInputArray(inputArray);
            var largestSum = GetLargestSum();
            return largestSum;
        }

        private void GetInputArray(int[] inputArray)
        {
            _inputArray = inputArray;
            Console.Write("\nInput array: ");
            foreach (var x in _inputArray) Console.Write($"{x} ");
        }

        private int GetLargestSum()
        {
            var actualSum = 0;
            var maxSum_i = 0;
            var maxSum_j = 0;

            var indexMax = _inputArray.Length;
            var indexLow_i = 0;
            var indexHigh_i = 0;
            var indexLow_j = 0;
            var indexHigh_j = 0;

            Console.WriteLine("\n\nSearch process:\n");

            for (var i = 0; indexHigh_i < indexMax; i++)
            {
                indexLow_i = 0;
                indexLow_j = 0;
                indexHigh_j = indexHigh_i;
                maxSum_j = 0;

                for (var j = 0; indexHigh_j < indexMax; j++)
                {
                    actualSum = GetSumOfSliceItems(indexLow_j, indexHigh_j);
                    if (actualSum > maxSum_j) maxSum_j = actualSum;

                    indexLow_j++;
                    indexHigh_j++;
                }
                Console.WriteLine($"Max sum for slice width = {indexHigh_i+1}: {maxSum_j}");
                if (maxSum_j > maxSum_i) maxSum_i = maxSum_j;
                indexHigh_i++;
            }
            Console.Write($"\nResult: {maxSum_i}\n");
            return maxSum_i;
        }

        private int GetSumOfSliceItems(int indexLow, int indexHigh)
        {
            var sumOfSliceItems = 0;
            for (var i = 0; i < _inputArray.Length; i++)
            {
                if (i >= indexLow && i <= indexHigh)
                    sumOfSliceItems += _inputArray[i];
            }
            return sumOfSliceItems;
        }
    }
}